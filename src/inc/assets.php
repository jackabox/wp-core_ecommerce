<?php // ==== ASSETS ==== //

if ( !function_exists( 'adtrak_enqueue_scripts' ) ) : function adtrak_enqueue_scripts() {

  $script_name = '';                // Empty by default, may be populated by conditionals below
  $script_vars = array();           // An empty array that can be filled with variables to send to front-end scripts
  $script_handle = 'adtrak';         // A generic script handle
  $suffix = '.min';                 // The suffix for minified scripts
  $ns = 'wp';                       // Namespace

  // Load original scripts when debug mode is on
  if ( WP_DEBUG === true )
    $suffix = '';

  $script_name = '-core';

  // Load theme-specific JavaScript bundles with versioning based on last modified time; http://www.ericmmartin.com/5-tips-for-using-jquery-with-wordpress/
  // The handle is the same for each bundle since we're only loading one script; if you load others be sure to provide a new handle
  wp_enqueue_script( $script_handle, get_stylesheet_directory_uri() . '/js/' . $ns . $script_name . $suffix . '.js', array( 'jquery' ), filemtime( get_template_directory() . '/js/' . $ns . $script_name . $suffix . '.js' ), true );

  // Pass variables to JavaScript at runtime; see: http://codex.wordpress.org/Function_Reference/wp_localize_script
  $script_vars = apply_filters( 'adtrak_script_vars', $script_vars );
  if ( !empty( $script_vars ) )
    wp_localize_script( $script_handle, 'adtrakVars', $script_vars );


  // Register and enqueue our main stylesheet with versioning based on last modified time
  wp_register_style( 'adtrak-style', get_stylesheet_uri(), $dependencies = array(), filemtime( get_template_directory() . '/style.css' ) );
  wp_enqueue_style( 'adtrak-style' );

} endif;
add_action( 'wp_enqueue_scripts', 'adtrak_enqueue_scripts' );
