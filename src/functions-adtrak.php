<?php
/**
 * functions and definitions
 * @package adtrak_blank
 */

# Include the files for Adtrak Customised.
require('inc/custom-dash.php');
require("inc/wp-head.php");

# Allow SVGs within the Theme
function allow_svg( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter( 'upload_mimes', 'allow_svg' );

# Customise the rendered images
function remove_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    return $html;
}

function my_image_class_filter() {
    return 'w100' ;
}

function stop_thumbs($sizes){
    return array();
}

add_filter('get_image_tag_class', 'my_image_class_filter');
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

# Strip the menu down (remove li classes)
function strip_menu($var) {
  return is_array($var) ? array_intersect($var, array('current-menu-item', 'current-menu-parent', 'current-type-parent')) : '';
}
add_filter('nav_menu_css_class', 'strip_menu', 100, 1);
add_filter('nav_menu_item_id', 'strip_menu', 100, 1);
add_filter('page_css_class', 'strip_menu', 100, 1);
