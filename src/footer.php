  </div>

  <footer id="colophon" class="site-footer" role="contentinfo">
    <div class="constrain">
      <nav id="site-footer-navigation" role="navigation">
        <?php wp_nav_menu(array('theme_location' => 'footer', 'menu_class' => 'menu-footer menu-inline')); ?>
      </nav>
    </div>
  </footer>

</div><!-- /page -->

<?php wp_footer(); ?>
</body>
</html>
