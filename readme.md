# About

This utilises the built theme, which is stripped down. [https://github.com/synapticism/wordpress-gulp-bower-sass](). I then included bower and neat, stripped out their sass and rebuilt it using one I've built over the years. See their documentation for how to roll with it.

If it doesn't work with Yeoman and symlinks, change the gulpfile to compile build to the wp-content/themes directory.

## Expanding on this to work with Ecommerce nicely. 

### Functionality
- ajax add to cart
- filtering system
- display of products
- acf advanced functions for global theme options

### Bower / Gulp
- include any dependancies here

etc.

## Dependancies

- [WooCommerce](http://www.woothemes.com/woocommerce/)


## To Do

- run a mock system (poppy blow) and use it to remove redundancies and set base elements that may be commonly needed
- check if system works, rapid enough, etc.
- look at a way to combine the included files.